main:
	git submodule update --init --recursive

update:
	git submodule update --recursive
	git submodule foreach --recursive git fetch
	git submodule foreach git pull --ff-only origin master
