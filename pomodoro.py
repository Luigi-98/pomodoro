#!/usr/bin/env python

import os
import argparse
import json
import signal
import time
from datetime import datetime, timedelta

whereami = os.path.dirname(__file__)

def ctrl_C_handler(*args):
	exit(0)

signal.signal(signal.SIGINT, ctrl_C_handler)

paths = [
	{
		'P': "/usr/share/sounds/freedesktop/stereo/complete.oga",
		'S':  "/usr/share/sounds/freedesktop/stereo/message-new-instant.oga"
	},
	"/usr/share/sounds/gnome/default/alerts/glass.ogg"
]

path = {}
for p in paths:
	if type(p) is str:
		if os.path.isfile(p):
			path = { key: p for key in ['P', 'S'] }
			break
	elif type(p) is dict:
		notfound = False
		for key in p:
			if not os.path.isfile(p[key]):
				notfound = True
				continue
		if not notfound:
			path = p
			break

if not path:
	print("No sound, sorry!")

def beep(status):
	if path:
		if not nosound:
			return os.system(f"paplay {path[status]}")
	return 2

def sleep(minutes):
	return time.sleep(int(minutes * 60))

urgentnotify = False
def notify(txt):
	urgent_argument = "-u critical" if urgentnotify else ""
	script_path = os.path.join(whereami, "./notify-send.sh/notify-send.sh")
	return os.system(f"'{script_path}' -f -t 3000 Pomodoro {urgent_argument} '{txt}'")

def get_time(delta=None):
	t = datetime.now()
	if type(delta) is timedelta:
		t += delta
	return t.strftime('%H:%M:%S')

lang = os.environ.get('LANG', 'en_US').split('.')[0]

lang_settings = os.path.join(os.path.dirname(__file__), f"langs/{lang}.conf")
if not os.path.isfile(lang_settings):
	# defaults to en_US
	lang_settings = os.path.join(os.path.dirname(__file__), f"langs/en_US.conf")
with open(lang_settings, 'r') as f:
	lang_settings = json.load(f)

messages = lang_settings.get('messages', {})
help = lang_settings.get('help', {})

def get_help(voice):
	"""
	Gets help message from lang settings.
	"""
	return help.get(voice, '')

def get_message(voice):
	"""
	Gets message from lang settings.
	"""
	return messages.get(voice, '')

parser = argparse.ArgumentParser(
	description=get_help('description'),
	epilog=get_help('e.g.') + "\n\t./pomodoro.py \"S25;P5;S25;P5\"",
	add_help=False
)
parser.add_argument('txt', type=str, metavar='schema', help=get_help('txt'))
parser.add_argument('-w', '--work', help=get_help('work'), action='store_true')
parser.add_argument('-ns', '--no-sound', help=get_help('no-sound'), action='store_true')
parser.add_argument('-un', '--urgent-notify', help=get_help('urgent-notify'), action='store_true')
parser.add_argument('-nv', '--not-verbose', help=get_help('not-verbose'), action='store_true')
parser.add_argument('-h', '--help', action='help', default=argparse.SUPPRESS, help=get_help('-h'))
args = parser.parse_args()
if 'txt' not in args:
	exit(-1)

txt = args.txt
work = args.work
nosound = args.no_sound
urgentnotify = args.urgent_notify
verbose = not args.not_verbose


def log(*args, **kwargs):
	if verbose:
		return print(*args, **kwargs)
	return


steps = txt.split(";")
steps = [ ( step[0], float(step[1:]) ) for step in steps ]

log("=====")
log(*steps, sep=",\n")
log("=====")

messages = {
	'S': get_message('work' if work else 'study'),
	'P': get_message('pause'),
	'theend': get_message('theend')
}

for tp, duration in steps:
	if tp in ('S', 'P'):
		txt = get_message(tp)
	else:
		txt = tp

	txt += f" ({duration} min)"
	
	beep(tp)
	notify(txt)
	log(tp, "Start:", get_time(), "\t\tEnd:", get_time(timedelta(minutes=duration)))
	sleep(duration)

beep('P')
notify(get_message('theend'))

exit(0)
